# SpamAssassin plugin to look up addresses in Roundcube addressbook.

=head1 NAME

RoundcubeAddressBook - look up addresses in Roundcube addressbook

=head1 SYNOPSIS

  loadplugin    Mail::SpamAssassin::Plugin::RoundcubeAddressBook
  header        __ROUNDCUBE_IN_ADDRESSBOOK eval:check_roundcube_addressbook()
  meta          ROUNDCUBE_IN_ADDRESSBOOK __ROUNDCUBE_IN_ADDRESSBOOK
  score         ROUNDCUBE_IN_ADDRESSBOOK -5

=cut

package Mail::SpamAssassin::Plugin::RoundcubeAddressBook;

use Mail::SpamAssassin::Plugin;
use strict;
use warnings;
use re 'taint';

use DBI;

our @ISA = qw(Mail::SpamAssassin::Plugin);

sub new {
  my ($class, $main) = @_;

  $class = ref($class) || $class;
  my $self = $class->SUPER::new($main);
  bless($self, $class);

  $self->{main} = $main;
  $self->{conf} = $main->{conf};
  $self->set_config($main->{conf});

  $self->register_eval_rule("check_roundcube_addressbook");

  return $self;
}

sub set_config {
  my ($self, $conf) = @_;
  my @cmds;

  push(@cmds, {
    setting => 'roundcube_db_dsn',
    default => '',
    type    => $Mail::SpamAssassin::Conf::CONF_TYPE_STRING
  });

  push(@cmds, {
    setting => 'roundcube_db_username',
    default => '',
    type    => $Mail::SpamAssassin::Conf::CONF_TYPE_STRING
  });

  push(@cmds, {
    setting => 'roundcube_db_password',
    default => '',
    type    => $Mail::SpamAssassin::Conf::CONF_TYPE_STRING
  });

  $conf->{parser}->register_commands(\@cmds);
}

sub open_db {
  my $self = shift;

  return 1 if defined ($self->{rcdbh});

  my $dsn = $self->{conf}->{roundcube_db_dsn};
  my $dbuser = $self->{conf}->{roundcube_db_username};
  my $dbpass = $self->{conf}->{roundcube_db_password};
  my $rcdbh = DBI->connect($dsn, $dbuser, $dbpass, {'PrintError' => 0});
  if (!$rcdbh) {
    info("roundcube-addressbook: unable to connect to sql database (%s) : %s",
         $dsn, DBI::errstr);
    return 0;
  }

  $self->{rcdbh} = $rcdbh;
  return 1;
}

sub is_sender_in_addressbook {
  my ($self, $sender) = @_;
  my $username = $self->{main}->{username};
  my $found = 0;

  my $sql = "SELECT 1 FROM contacts AS c LEFT JOIN users AS u ON c.user_id = u.user_id WHERE u.username = ? AND c.email = ?";
  my @args = ($username, $sender);
  my $sth = $self->{rcdbh}->prepare($sql);
  my $rc = $sth->execute(@args);
  if ($rc) {
    my $aryref;
    while (defined($aryref = $sth->fetchrow_arrayref())) {
      $found = 1;
      last;
    }
  }
  $sth->finish();
  return $found;
}

sub check_roundcube_addressbook {
  my ($self, $pms) = @_;

  if (!$self->open_db()) {
    return 0;
  }

  my $in_addressbook = 0;
  foreach my $sender ($pms->all_from_addrs()) {
    if ($self->is_sender_in_addressbook($sender)) {
      $in_addressbook = 1;
      last;
    }
  }

  return $in_addressbook;
}

1;
